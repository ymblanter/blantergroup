---
title: "Blanter Group - Openings"
layout: textlay
excerpt: "Openings"
sitemap: false
permalink: /openings/
---

# Open positions

<!-- **We will have a PhD position available in 2023 on microwave sensing using spins in diamond related to our [Quantum Technology grant](https://www.nwo.nl/en/news/sixteen-awards-ngf-quantum-technology-programme).** -->

### Postdoc

Currently we have no open postdoc positions.

### PhD
<!-- If you are interested in working with us as a PhD student or postdoc, please send me an [email](mailto:t.vandersar@tudelft.nl). State briefly why you are interested and attach a CV, including information about the grades you had as an undergraduate. No need for separate certificates. If you are applying to a specific advertisement, note this in your email.

We welcome postdocs with fellowships. I'd be happy to support you, also after you apply to our group. Take a look at the [veni fellowship](https://www.nwo.nl/en/calls/nwo-talent-programme-veni-science-domain) or the Marie Curie fellowship, [here is a previous call]({{ site.baseurl }}/downloads/h2020-wp1820-msca_en.pdf)). In many countries, there are fellowships available for outgoing postdocs.** -->

Currently we have no open PhD positions.

### Master/bachelor thesis projects
For information on possible MSc or BSc thesis projects, please contact [Yaroslav](mailto:y.m.blanter@tudelft.nl).

Note that masters and bachelors projects are only available to students already enrolled in a bachelors or masters program at a university in the Netherlands. For information about enrolling for the bachelors or masters program at TU Delft, please consult the TU-Delft homepage.

<p align="center">  
    <img src="{{ site.url }}{{ site.baseurl }}/images/picpic/Gallery/realfun.png" width="20%">
</p>

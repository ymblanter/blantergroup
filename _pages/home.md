---
title: "Blanter Group - Home"
layout: homelay
excerpt: "Blanter Group at Delft University of Technology."
sitemap: false
permalink: /
---

We are a theory group focused on properties of quantum nanostructures and hybrid systems. We develop models aiming at prediction of novel phenomena and at explanation of existing experiments.

<p align="center">  
    <img src="{{ site.url }}{{ site.baseurl }}/images/picpic/Gallery/theory2.png" width="80%">
</p>



We are located at [Delft University of Technology](https://www.tudelft.nl/), home to Nobel Laureates Jacobus Henricus van ‘t Hoff and Simon van der Meer, amongst many other great scientists. We are part of the [Department of Quantum Nanoscience](http://qn.tudelft.nl/), as well as the [Kavli Institute of Nanoscience](http://kavli.tudelft.nl/).

<!-- **We are looking for talented and passionate Master students. You can find more information here!** [(more info)]({{ site.url }}{{ site.baseurl }}/openings) -->

<!-- We are grateful for funding from Delft University of Technology, [NWO](https://www.nwo.nl), the [Frontiers in Nanoscience program](https://casimir.researchschool.nl/nanofront-1962.html), and the [Kavli Foundation](http://www.kavlifoundation.org/). -->

<figure class="fourth">
  <img src="{{ site.url }}{{ site.baseurl }}/images/logopic/Logo_TUDelft.jpg" style="width: 285px">
  <img src="{{ site.url }}{{ site.baseurl }}/images/logopic/Logo_Kavli.jpg" style="width: 285px">
  <!-- <img src="{{ site.url }}{{ site.baseurl }}/images/logopic/Logo_Nanofront.jpg" style="width: 100px">
  <img src="{{ site.url }}{{ site.baseurl }}/images/logopic/Logo_NWO.jpg" style="width: 80px"> -->
</figure>

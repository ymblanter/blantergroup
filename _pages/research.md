---
title: "Blanter Group - Research"
layout: textlay
excerpt: "Blanter Group -- Research"
sitemap: false
permalink: /research/
---

# Research

**Cavity and Quantum Magnonics**

**Two-dimensional van der Waals Magnets**

**Electron Transport in Hopping Systems**

**Josephson Effect with non-trivial Materials**

<p align="center">  
    <img src="{{ site.url }}{{ site.baseurl }}/images/picpic/Gallery/theory1.png" width="50%">
</p>

